﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(this.textBoxUsername.Text))
            {
                MessageBox.Show("Please Enter Username first");
            }
            else
            {
                string username = this.textBoxUsername.Text.Trim();
                string password = this.maskedTextBoxPassword.Text.Trim();

                try
                {
                    AuthenticationManager.Instance.Authenticate(username, password);


                    this.DialogResult = System.Windows.Forms.DialogResult.OK;

                    var formMain = new FormMain();

                    this.Hide();
                    formMain.ShowDialog();
                    this.Show();
                }
                catch(Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
            }
        }
    }
}
