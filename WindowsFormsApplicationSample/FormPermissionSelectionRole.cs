﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    class FormPermissionSelectionRole : FormPermissionSelection
    {
        private int roleId;

        public FormPermissionSelectionRole(int roleId)
            : base()
        {
            this.roleId = roleId;

            var permissionsRole = AuthorizationManager.Instance.GetRolePermissions(roleId);
            var permissionsAll = AuthorizationManager.Instance.Permissions;

            var permissionSelections = permissionsAll.Select(permission =>
            new PermissionSelection()
            {
                Permission = permission,
                IsChangable = true,
                Description = string.Empty,
                IsSelected = permissionsRole.Contains(permission)

            });

            this.permissionSelections = permissionSelections;

            if(!AuthorizationManager.Instance.HasPermission("system.role.permission.update"))
            {
                this.dataGridViewPermissions.Columns["ColumnSelected"].ReadOnly = true;
                base.buttonSave.Enabled = false;
            }
        }

        protected override void Save()
        {
            var rolePermissionsSelectedIds = this.dataGridViewPermissions.Rows.OfType<DataGridViewRow>()
                .Where(row => (bool)row.Cells["ColumnSelected"].Value == true)
                .Select(row => (row.Tag as PermissionSelection).Permission.Id);

            AuthorizationManager.Instance.UpdateRolePermissions(this.roleId, rolePermissionsSelectedIds);
        }
    }
}
