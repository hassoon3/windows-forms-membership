﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            if (!AuthorizationManager.Instance.HasPermission("system.user.read"))
            {
                this.usersToolStripMenuItem.Enabled = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.user.update"))
            {
                this.resetPasswordToolStripMenuItem.Enabled = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.role.read"))
            {
                this.rolesToolStripMenuItem.Enabled = false;
            }
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formUsers = new FormUsers();
            formUsers.ShowDialog();
        }

        private void rolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formRoles = new FormRoles();
            formRoles.ShowDialog();
        }

        private void resetPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formSetPassword = new FormSetPassword();
            formSetPassword.ShowDialog();
        }
    }
}
