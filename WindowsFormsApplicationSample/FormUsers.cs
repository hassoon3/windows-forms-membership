﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormUsers : Form
    {
        public FormUsers()
        {
            InitializeComponent();

            if (!AuthorizationManager.Instance.HasPermission("system.user.create"))
            {
                this.buttonAddNew.Enabled = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.user.update"))
            {
                this.dataGridViewUsers.Columns["ColumnUpdate"].Visible = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.user.delete"))
            {
                this.dataGridViewUsers.Columns["ColumnDelete"].Visible = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.user.permission.read"))
            {
                this.dataGridViewUsers.Columns["ColumnPermissions"].Visible = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.user.role.read"))
            {
                this.dataGridViewUsers.Columns["ColumnRoles"].Visible = false;
            }
        }

        private void FormUsers_Load(object sender, EventArgs e)
        {
            this.reloadUsers();
        }

        private void reloadUsers()
        {
            var users = AuthenticationManager.Instance.Users;

            this.dataGridViewUsers.Rows.Clear();

            users.ToList().ForEach(user =>
            {
                int newRowIndex = dataGridViewUsers.Rows.Add(user.Name, user.Username);
                dataGridViewUsers.Rows[newRowIndex].Tag = user.Id;
            });
        }

        private void dataGridViewUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (e.RowIndex >= 0)
            {
                var columns = senderGrid.Columns;

                if (columns["ColumnRoles"].Index == e.ColumnIndex)
                {
                    this.dataGridViewUsers_ColumnRolesClick(sender, e);
                }
                else if (columns["ColumnPermissions"].Index == e.ColumnIndex)
                {
                    this.dataGridViewUsers_ColumnPermissionsClick(sender, e);
                }
                else if (columns["ColumnUpdate"].Index == e.ColumnIndex)
                {
                    this.dataGridViewUsers_ColumnUpdateClick(sender, e);
                }
                else if (columns["ColumnDelete"].Index == e.ColumnIndex)
                {
                    this.dataGridViewUsers_ColumnDeleteClick(sender, e);
                }
            }
        }

        private void dataGridViewUsers_ColumnRolesClick(object sender, DataGridViewCellEventArgs e)
        {
            var userId = dataGridViewUsers.Rows[e.RowIndex].Tag;

            var formRoleSelect = new FormRoleSelection((int)userId);

            formRoleSelect.ShowDialog();
        }

        private void dataGridViewUsers_ColumnPermissionsClick(object sender, DataGridViewCellEventArgs e)
        {
            var userId = dataGridViewUsers.Rows[e.RowIndex].Tag;

            var formPermissionSelect = new FormPermissionSelectionUser((int)userId);

            formPermissionSelect.ShowDialog();
        }

        private void dataGridViewUsers_ColumnUpdateClick(object sender, DataGridViewCellEventArgs e)
        {
            var dataGridViewUsers = sender as DataGridView;

            var row = dataGridViewUsers.Rows[e.RowIndex];
            var userId = (int)row.Tag;

            var formUser = new FormUser(userId);
            formUser.ShowDialog();

            this.reloadUsers();
        }

        private void dataGridViewUsers_ColumnDeleteClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete the user?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                var dataGridViewUsers = sender as DataGridView;

                var row = dataGridViewUsers.Rows[e.RowIndex];
                var userId = (int)row.Tag;

                try
                {
                    AuthenticationManager.Instance.DeleteUser(userId);

                    MessageBox.Show("User deleted successfully");

                    this.reloadUsers();
                }
                catch(Exception exp)
                {
                    MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddNew_Click(object sender, EventArgs e)
        {
            var formUser = new FormUser();
            formUser.ShowDialog();

            this.reloadUsers();
        }
    }
}
