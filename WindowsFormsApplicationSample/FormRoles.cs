﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormRoles : Form
    {
        public FormRoles()
        {
            InitializeComponent();

            if(!AuthorizationManager.Instance.HasPermission("system.role.create"))
            {
                this.buttonAddNew.Enabled = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.role.update"))
            {
                this.dataGridViewRoles.Columns["ColumnUpdate"].Visible = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.role.delete"))
            {
                this.dataGridViewRoles.Columns["ColumnDelete"].Visible = false;
            }

            if (!AuthorizationManager.Instance.HasPermission("system.role.permission.read"))
            {
                this.dataGridViewRoles.Columns["ColumnPermissions"].Visible = false;
            }
        }

        private void FormRoles_Load(object sender, EventArgs e)
        {
            reloadRoles();
        }

        private void reloadRoles()
        {
            dataGridViewRoles.Rows.Clear();

            var roles = AuthorizationManager.Instance.Roles;

            roles.ToList().ForEach(role =>
            {
                int newRowIndex = dataGridViewRoles.Rows.Add(role.Name);
                dataGridViewRoles.Rows[newRowIndex].Tag = role.Id;
            });
        }

        private void dataGridViewRoles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (e.RowIndex >= 0)
            {
                var columns = senderGrid.Columns;

                if (columns["ColumnPermissions"].Index == e.ColumnIndex)
                {
                    this.dataGridViewRoles_ColumnPermissionsClick(sender, e);
                }
                else if (columns["ColumnUpdate"].Index == e.ColumnIndex)
                {
                    this.dataGridViewRoles_ColumnUpdateClick(sender, e);
                }
                else if (columns["ColumnDelete"].Index == e.ColumnIndex)
                {
                    this.dataGridViewRoles_ColumnDeleteClick(sender, e);
                }
            }
        }

        private void dataGridViewRoles_ColumnPermissionsClick(object sender, DataGridViewCellEventArgs e)
        {
            var roleId = dataGridViewRoles.Rows[e.RowIndex].Tag;

            var formPermissionSelect = new FormPermissionSelectionRole((int)roleId);

            formPermissionSelect.ShowDialog();
        }

        private void dataGridViewRoles_ColumnUpdateClick(object sender, DataGridViewCellEventArgs e)
        {
            var dataGridViewRoles = sender as DataGridView;

            var row = dataGridViewRoles.Rows[e.RowIndex];
            var roleId = (int)row.Tag;

            var formRole = new FormRole(roleId);
            formRole.ShowDialog();

            this.reloadRoles();
        }

        private void dataGridViewRoles_ColumnDeleteClick(object sender, DataGridViewCellEventArgs e)
        {
            if(MessageBox.Show("Are you sure you want to delete this role?","Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    var grid = (DataGridView)sender;

                    var roleId = (int)grid.Rows[e.RowIndex].Tag;

                    AuthorizationManager.Instance.DeleteRole(roleId);

                    MessageBox.Show("Roles deleted successfully");

                    this.reloadRoles();
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddNew_Click(object sender, EventArgs e)
        {
            var formRole = new FormRole();
            formRole.ShowDialog();
            
            this.reloadRoles();
        }
    }
}
