﻿namespace WindowsFormsApplicationSample
{
    partial class FormUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewUsers = new System.Windows.Forms.DataGridView();
            this.buttonDone = new System.Windows.Forms.Button();
            this.buttonAddNew = new System.Windows.Forms.Button();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUsernam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnRoles = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnPermissions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnUpdate = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColumnDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewUsers
            // 
            this.dataGridViewUsers.AllowUserToAddRows = false;
            this.dataGridViewUsers.AllowUserToDeleteRows = false;
            this.dataGridViewUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnName,
            this.ColumnUsernam,
            this.ColumnRoles,
            this.ColumnPermissions,
            this.ColumnUpdate,
            this.ColumnDelete});
            this.dataGridViewUsers.Location = new System.Drawing.Point(11, 12);
            this.dataGridViewUsers.Name = "dataGridViewUsers";
            this.dataGridViewUsers.ReadOnly = true;
            this.dataGridViewUsers.RowTemplate.Height = 24;
            this.dataGridViewUsers.Size = new System.Drawing.Size(563, 222);
            this.dataGridViewUsers.TabIndex = 0;
            this.dataGridViewUsers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUsers_CellContentClick);
            // 
            // buttonDone
            // 
            this.buttonDone.Location = new System.Drawing.Point(11, 241);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(66, 34);
            this.buttonDone.TabIndex = 1;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = true;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // buttonAddNew
            // 
            this.buttonAddNew.Location = new System.Drawing.Point(484, 240);
            this.buttonAddNew.Name = "buttonAddNew";
            this.buttonAddNew.Size = new System.Drawing.Size(90, 34);
            this.buttonAddNew.TabIndex = 3;
            this.buttonAddNew.Text = "Add New ...";
            this.buttonAddNew.UseVisualStyleBackColor = true;
            this.buttonAddNew.Click += new System.EventHandler(this.buttonAddNew_Click);
            // 
            // ColumnName
            // 
            this.ColumnName.HeaderText = "Name";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 72;
            // 
            // ColumnUsernam
            // 
            this.ColumnUsernam.HeaderText = "Username";
            this.ColumnUsernam.Name = "ColumnUsernam";
            this.ColumnUsernam.ReadOnly = true;
            this.ColumnUsernam.Width = 98;
            // 
            // ColumnRoles
            // 
            this.ColumnRoles.HeaderText = "Roles";
            this.ColumnRoles.Name = "ColumnRoles";
            this.ColumnRoles.ReadOnly = true;
            this.ColumnRoles.Text = "Update";
            this.ColumnRoles.UseColumnTextForButtonValue = true;
            this.ColumnRoles.Width = 46;
            // 
            // ColumnPermissions
            // 
            this.ColumnPermissions.HeaderText = "Permissions";
            this.ColumnPermissions.Name = "ColumnPermissions";
            this.ColumnPermissions.ReadOnly = true;
            this.ColumnPermissions.Text = "Update";
            this.ColumnPermissions.UseColumnTextForButtonValue = true;
            this.ColumnPermissions.Width = 84;
            // 
            // ColumnUpdate
            // 
            this.ColumnUpdate.HeaderText = "Edit";
            this.ColumnUpdate.Name = "ColumnUpdate";
            this.ColumnUpdate.ReadOnly = true;
            this.ColumnUpdate.Text = "Edit";
            this.ColumnUpdate.UseColumnTextForButtonValue = true;
            this.ColumnUpdate.Width = 37;
            // 
            // ColumnDelete
            // 
            this.ColumnDelete.HeaderText = "Delete";
            this.ColumnDelete.Name = "ColumnDelete";
            this.ColumnDelete.ReadOnly = true;
            this.ColumnDelete.Text = "Delete";
            this.ColumnDelete.UseColumnTextForButtonValue = true;
            this.ColumnDelete.Width = 52;
            // 
            // FormUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 287);
            this.Controls.Add(this.buttonAddNew);
            this.Controls.Add(this.buttonDone);
            this.Controls.Add(this.dataGridViewUsers);
            this.Name = "FormUsers";
            this.Text = "Users";
            this.Load += new System.EventHandler(this.FormUsers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewUsers;
        private System.Windows.Forms.Button buttonDone;
        private System.Windows.Forms.Button buttonAddNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUsernam;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnRoles;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnPermissions;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnUpdate;
        private System.Windows.Forms.DataGridViewButtonColumn ColumnDelete;
    }
}