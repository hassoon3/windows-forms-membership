﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormSetPassword : Form
    {
        public FormSetPassword()
        {
            InitializeComponent();
        }

        private void buttonSet_Click(object sender, EventArgs e)
        {
            try
            {
                string passwordNew = this.textBoxNewPassword.Text.Trim();
                string passwordConfirm = this.textBoxConfirm.Text.Trim();

                if (!passwordNew.Equals(passwordConfirm))
                    throw new Exception("Passwords do NOT match");

                AuthenticationManager.Instance.SetPassword(passwordNew);

                MessageBox.Show("Success");

                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
