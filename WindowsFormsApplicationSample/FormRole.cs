﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormRole : Form
    {
        private class Role : INotifyPropertyChanged
        {
            public int? Id { get; set; }

            private string name;
            public string Name
            {
                get { return this.name; }
                set
                {
                    this.name = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("Name"));
                }
            }

            private void InvokePropertyChanged(PropertyChangedEventArgs e)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, e);
            }

            public event PropertyChangedEventHandler PropertyChanged;
        }

        private Role role;

        public FormRole()
        {
            InitializeComponent();

            this.role = new Role();
        }

        public FormRole(int id)
        {
            InitializeComponent();

            try
            {
                var role = AuthorizationManager.Instance.Roles.SingleOrDefault(r => r.Id == id);
                if(role == null)
                {
                    throw new Exception("Role Id does not exist");
                }

                this.role = new Role()
                {
                    Id = role.Id,
                    Name = role.Name
                };
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.Close();
            }
        }

        private void FormRole_Load(object sender, EventArgs e)
        {
            textBoxName.DataBindings.Add("Text", this.role, "Name");
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.role.Id.HasValue && this.role.Id != 0)
                {
                    this.saveExisting();
                }
                else
                {
                    this.saveNew();
                }


                MessageBox.Show("Role saved successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void saveNew()
        {
            if (string.IsNullOrWhiteSpace(this.role.Name))
            {
                throw new Exception("Name is empty!");
            }

            AuthorizationManager.Instance.CreateRole(
                this.role.Name);
        }

        private void saveExisting()
        {
            if (string.IsNullOrWhiteSpace(this.role.Name))
            {
                throw new Exception("Name is empty!");
            }

            AuthorizationManager.Instance.UpdateRole(
                this.role.Id.Value,
                this.role.Name);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
