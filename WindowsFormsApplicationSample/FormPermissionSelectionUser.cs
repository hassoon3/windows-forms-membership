﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    class FormPermissionSelectionUser : FormPermissionSelection
    {
        private int userId;

        public FormPermissionSelectionUser(int userId)
            : base()
        {
            this.userId = userId;

            var permissionsUser = AuthorizationManager.Instance.GetUserPermissions(userId);
            var rolesUser = AuthorizationManager.Instance.GetUserRoles(userId);
            var permissionsAll = AuthorizationManager.Instance.Permissions;

            var permissionSelections = permissionsAll.Select(permission =>
                {
                    var rolesContainPermission = rolesUser.Where(role => role.Permissions.ToList().Contains(permission));
                    return new PermissionSelection()
                    {
                        Permission = permission,
                        IsChangable = !rolesUser.Any(role => role.Permissions.ToList().Contains(permission)),
                        Description = (rolesContainPermission.Count() > 0 ? "In roles: " : string.Empty) + string.Join(", ", rolesContainPermission.Select(role => role.Name).ToArray()),
                        IsSelected = rolesUser.Any(role => role.Permissions.ToList().Contains(permission)) || permissionsUser.ToList().Contains(permission)
                    };
                });

            this.permissionSelections = permissionSelections;

            if (!AuthorizationManager.Instance.HasPermission("system.user.permission.update"))
            {
                this.dataGridViewPermissions.Columns["ColumnSelected"].ReadOnly = true;
                base.buttonSave.Enabled = false;
            }
        }

        protected override void Save()
        {
            var rolePermissionsSelectedIds = this.dataGridViewPermissions.Rows.OfType<DataGridViewRow>()
                .Where(row => (bool)row.Cells["ColumnSelected"].Value == true)
                .Where(row => (row.Tag as PermissionSelection).IsChangable)
                .Select(row => (row.Tag as PermissionSelection).Permission.Id);

            AuthorizationManager.Instance.UpdateUserPermissions(this.userId, rolePermissionsSelectedIds);
        }
    }
}
