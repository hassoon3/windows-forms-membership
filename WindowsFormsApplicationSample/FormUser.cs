﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormUser : Form
    {
        private class User : INotifyPropertyChanged
        {
            public int? Id { get; set; }

            private string name;
            public string Name
            {
                get { return this.name; }
                set
                {
                    this.name = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("Name"));
                }
            }

            private string username;
            public string Username
            {
                get { return this.username; }
                set
                {
                    this.username = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("Username"));
                }
            }

            private bool changePassword;
            public bool ChangePassword
            {
                get { return this.changePassword; }
                set
                {
                    this.changePassword = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("ChangePassword"));
                }
            }

            private string password;
            public string Password
            {
                get { return this.password; }
                set
                {
                    this.password = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("Password"));
                }
            }

            private string passwordConfirm;
            public string PasswordConfirm
            {
                get { return this.passwordConfirm; }
                set
                {
                    this.passwordConfirm = value;
                    InvokePropertyChanged(new PropertyChangedEventArgs("PasswordConfirm"));
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            private void InvokePropertyChanged(PropertyChangedEventArgs e)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, e);
            }

        }

        private User user;

        public FormUser()
        {
            InitializeComponent();

            checkBoxChangePassword.Checked = true;
            checkBoxChangePassword.Enabled = false;

            this.user = new User() { ChangePassword = true };
        }

        public FormUser(int id)
        {
            InitializeComponent();

            maskedTextBoxPassword.Enabled = false;
            maskedTextBoxPasswordConfirm.Enabled = true;

            try
            {
                var user = AuthenticationManager.Instance.Users.SingleOrDefault(u => u.Id == id);
                if(user == null)
                {
                    throw new Exception("User Id does not exist");
                }

                this.user = new User()
                {
                    Id = user.Id,
                    Name = user.Name,
                    Username = user.Username,
                    ChangePassword = false
                };
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.Close();
            }
        }

        private void FormUser_Load(object sender, EventArgs e)
        {
            textBoxName.DataBindings.Add("Text", this.user, "Name");
            textBoxUsername.DataBindings.Add("Text", this.user, "Username");

            checkBoxChangePassword.DataBindings.Add("Checked", this.user, "ChangePassword", true, DataSourceUpdateMode.OnPropertyChanged);

            maskedTextBoxPassword.DataBindings.Add("Text", this.user, "Password");
            maskedTextBoxPasswordConfirm.DataBindings.Add("Text", this.user, "PasswordConfirm");

            maskedTextBoxPassword.DataBindings.Add("Enabled", this.user, "ChangePassword");
            maskedTextBoxPasswordConfirm.DataBindings.Add("Enabled", this.user, "ChangePassword");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(this.user.Id.HasValue && this.user.Id != 0)
                {
                    this.saveExisting();
                }
                else
                {
                    this.saveNew();
                }
                
                
                MessageBox.Show("User saved successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void saveExisting()
        {
            if (string.IsNullOrWhiteSpace(this.user.Name))
            {
                throw new Exception("Name is empty!");
            }
            if (string.IsNullOrWhiteSpace(this.user.Username))
            {
                throw new Exception("Username is empty!");
            }

            AuthenticationManager.Instance.UpdateUser(
                this.user.Id.Value,
                this.user.Name,
                this.user.Username,
                this.user.ChangePassword? (this.user.Password == null? string.Empty : this.user.Password) : null);
        }

        private void saveNew()
        {
            //Password can be empty

            //Checking Password for match
            if (this.user.Password != this.user.PasswordConfirm)
            {
                throw new Exception("Passwords do not match");
            }
            if (string.IsNullOrWhiteSpace(this.user.Name))
            {
                throw new Exception("Name is empty!");
            }
            if (string.IsNullOrWhiteSpace(this.user.Username))
            {
                throw new Exception("Username is empty!");
            }

            AuthenticationManager.Instance.CreateUser(
                this.user.Name, 
                this.user.Username, 
                this.user.Password);
        }
    }
}
