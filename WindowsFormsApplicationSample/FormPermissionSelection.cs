﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormPermissionSelection : Form
    {
        protected class PermissionSelection
        {
            public Permission Permission { get; set; }
            public bool IsSelected { get; set; }
            public bool IsChangable { get; set; }
            public string Description { get; set; }
        }

        protected IEnumerable<PermissionSelection> permissionSelections;

        public FormPermissionSelection()
        {
            InitializeComponent();
        }

        private void FormPermissionSelect_Load(object sender, EventArgs e)
        {
            this.permissionSelections.ToList().ForEach(permissionSelection =>
            {
                int newRowIndex = dataGridViewPermissions.Rows.Add(permissionSelection.IsSelected, permissionSelection.Permission.Description, permissionSelection.Description);
                dataGridViewPermissions.Rows[newRowIndex].Tag = permissionSelection;

                bool readOnly = dataGridViewPermissions.Rows[newRowIndex].Cells["ColumnSelected"].ReadOnly;
                dataGridViewPermissions.Rows[newRowIndex].Cells["ColumnSelected"].ReadOnly = readOnly || !permissionSelection.IsChangable;
            });
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            this.Save();
            MessageBox.Show("Permissions saved successfully");
            this.Close();
        }

        protected virtual void Save()
        {
            throw new NotImplementedException();
        }
    }
}
