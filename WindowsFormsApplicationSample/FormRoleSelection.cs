﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsMembership;

namespace WindowsFormsApplicationSample
{
    public partial class FormRoleSelection : Form
    {
        protected class RoleSelection
        {
            public Role Role { get; set; }
            public bool IsSelected { get; set; }
        }

        private int userId;

        public FormRoleSelection(int userId)
        {
            InitializeComponent();

            this.userId = userId;

            if(!AuthorizationManager.Instance.HasPermission("system.role.permission.update"))
            {
                this.dataGridViewRoles.Columns["ColumnSelected"].ReadOnly = true;
                this.buttonSave.Enabled = false;
            }
        }

        private void FormRoleSelection_Load(object sender, EventArgs e)
        {
            //Generate Role Selections
            var rolesAll = AuthorizationManager.Instance.Roles;
            var rolesUser = AuthorizationManager.Instance.GetUserRoles(userId);

            var roleSelections = from role in rolesAll
                                 select new RoleSelection()
                                 {
                                     Role = role,
                                     IsSelected = rolesUser.Contains(role)
                                 };

            //Bind Role Selections
            roleSelections.ToList().ForEach(roleSelection =>
            {
                var newRowIndex = this.dataGridViewRoles.Rows.Add(roleSelection.IsSelected, roleSelection.Role.Name);
                this.dataGridViewRoles.Rows[newRowIndex].Tag = roleSelection;
            });
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            var rolesSelectedIds = this.dataGridViewRoles.Rows.OfType<DataGridViewRow>()
                .Where(row => (bool)row.Cells["ColumnSelected"].Value == true)
                .Select(row => (row.Tag as RoleSelection).Role.Id);

            AuthorizationManager.Instance.UpdateUserRoles(this.userId, rolesSelectedIds);

            MessageBox.Show("Roles saved successfully.","Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
