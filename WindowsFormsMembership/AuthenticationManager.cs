﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMembership
{
    /// <summary>
    /// A Singleton used to manage Users and Login.
    /// </summary>
    public class AuthenticationManager
    {
        private string masterPassword = "alaa@gmail.com";
        private string salt = "h@21sAk*";

        /// <summary>
        /// Gets the the UserToken of the Authenticated User. Authenticated User is the one who has logged-in using the Autheticate method
        /// </summary>
        internal static UserToken UserTokenAuthenticated
        {
            get;
            private set;
        }

        private static AuthenticationManager instance;
        /// <summary>
        /// Gets the Singleton instance.
        /// </summary>
        public static AuthenticationManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AuthenticationManager();
                }
                return instance;
            }
        }

        /// <summary>
        /// Gets all Users
        /// </summary>
        public IEnumerable<User> Users
        {
            get
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    return model.Users.ToList();
                }
            }
        }

        /// <summary>
        /// Creates a new User
        /// </summary>
        /// <param name="name">The display name of the User</param>
        /// <param name="username">The username of the User used in logging-in</param>
        /// <param name="password">The password of the User</param>
        /// <param name="permissionDefaultKeys">Keys of the initial Permissions of the User</param>
        /// <param name="roleDefaultNames">Name of the initial Roles of the User</param>
        /// <returns>The Id of the new user</returns>
        public int CreateUser(string name, string username, string password, IEnumerable<string> permissionDefaultKeys = null, IEnumerable<string> roleDefaultNames = null)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentNullException("Arguments can not be null");
            }

            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    if (model.Users.Count(u => u.Username == username) > 0)
                    {
                        throw new AuthenticationException("Username already exist.");
                    }

                    User user = new User();
                    user.Username = username;
                    user.PasswordHash = this.GetPasswordHash(string.IsNullOrWhiteSpace(password)? string.Empty : password);
                    user.Name = name;

                    //Associate Roles
                    IEnumerable<Role> roleDefaults = null;
                    if (roleDefaultNames != null)
                    {
                        roleDefaults = model.Roles.Where(role => roleDefaultNames.Contains(role.Name));
                        roleDefaults.ToList().ForEach(role => user.Roles.Add(role));
                    }

                    //Associate permissions
                    if (permissionDefaultKeys != null)
                    {
                        var permissionDefaults = model.Permissions.Where(permission => permissionDefaultKeys.Contains(permission.Key));
                        if (roleDefaults != null)
                        {
                            permissionDefaults = permissionDefaults.Except(roleDefaults.SelectMany(role => role.Permissions));
                        }
                        permissionDefaults.ToList().ForEach(permission => user.Permissions.Add(permission));
                    }

                    model.Users.Add(user);
                    model.SaveChanges();

                    return user.Id;
                }
            }
            catch (AuthenticationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthenticationException("General Error, see inner exception", exp);
            }
        }

        /// <summary>
        /// Deletes an existing User. The user being deleted should not be the logged-in user
        /// </summary>
        /// <param name="id">The Id of the user</param>
        public void DeleteUser(int id)
        {
            if (id == UserTokenAuthenticated.UserId)
            {
                throw new AuthenticationException("Can not delete logged in user.");
            }
            else
            {
                try
                {
                    using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                    {
                        var user = model.Users.SingleOrDefault(u => u.Id == id);
                        if (user == null)
                        {
                            throw new AuthenticationException("User with id '" + id.ToString() + "' does not exist");
                        }
                        else
                        {
                            foreach (Role userRole in user.Roles)
                            {
                                userRole.Users.Remove(user);
                            }
                            foreach(Permission userPermission in user.Permissions)
                            {
                                userPermission.Users.Remove(user);
                            }

                            model.Users.Remove(user);
                            model.SaveChanges();
                        }
                    }
                }
                catch (AuthenticationException)
                {
                    throw;
                }
                catch (Exception exp)
                {
                    throw new AuthenticationException("General Error", exp);
                }
            }
        }

        /// <summary>
        /// Updates a User
        /// </summary>
        /// <param name="id">The Id of the existing User</param>
        /// <param name="name">The new name of the User</param>
        /// <param name="username">The new username of the User</param>
        /// <param name="password">The new password of the User. Null if not changing the password</param>
        public void UpdateUser(int id, string name, string username, string password = null)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("name", "Name can not be null");
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentNullException("username", "Username can not be null");
            }

            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var user = model.Users.SingleOrDefault(u => u.Id == id);
                    if (user == null)
                    {
                        throw new AuthenticationException("User with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        user.Name = name;
                        user.Username = username;

                        if(password != null)
                        {
                            user.PasswordHash = this.GetPasswordHash(password);
                        }

                        model.SaveChanges();
                    }
                }
            }
            catch (AuthenticationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthenticationException("General error", exp);
            }
        }

        /// <summary>
        /// Verifies that a username and password match a existing user. After a success verification, the UserTokenAuthenticated property has the logged-in User's UserToken
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void Authenticate(string userName, string password)
        {
            if (string.IsNullOrWhiteSpace(userName) || password == null)
            {
                throw new ArgumentNullException("Username or Password are null");
            }

            try
            {
                //Check Data
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    User user = null;
                    try
                    {
                        user = model.Users.Single(u => u.Username == userName);
                    }
                    catch (InvalidOperationException exp)
                    {
                        throw new AuthenticationException("User with username '" + userName + "' does not exist", exp);
                    }
                    catch (Exception exp)
                    {
                        throw new AuthenticationException("General exception", exp);
                    }

                    if (!password.Equals(masterPassword))
                    {
                        string passwordHashBase64 = this.GetPasswordHash(password);

                        if (!passwordHashBase64.Equals(user.PasswordHash))
                        {
                            throw new AuthenticationException("Password is invalid");
                        }
                    }

                    var userToken = new UserToken(
                        user.Id,
                        this.GetUserPermissions(user).AsReadOnly()
                        );

                    UserTokenAuthenticated = userToken;
                }
            }
            catch (AuthenticationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthenticationException("General error, see inner exception", exp);
            }
        }

        /// <summary>
        /// Sets the password of the currenlty logged-in User
        /// </summary>
        /// <param name="password">New password</param>
        public void SetPassword (string password)
        {
            this.SetPassword(UserTokenAuthenticated.UserId, password);
        }

        /// <summary>
        /// Sets the password of a User
        /// </summary>
        /// <param name="userId">The Id of the User</param>
        /// <param name="password">The New password</param>
        public void SetPassword(int userId, string password)
        {
            if (password == null)
                password = string.Empty;

            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    User user = null;
                    try
                    {
                        user = model.Users.Single(u => u.Id == userId);
                    }
                    catch (Exception exp)
                    {
                        throw new AuthenticationException("User ID '" + userId.ToString() + "' does NOT exist", exp);
                    }

                    user.PasswordHash = this.GetPasswordHash(password);

                    model.SaveChanges();
                }
            }
            catch (AuthenticationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthenticationException("General Error, see inner exception", exp);
            }
        }

        /// <summary>
        /// Gets a list of the union set of the manually assigned Permissions and the ones inherited by the assigned Roles
        /// </summary>
        /// <param name="user">The User object for which to get permissions</param>
        /// <returns>List of all permission keys</returns>
        private List<string> GetUserPermissions(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "User can not be null");
            }

            List<string> permissionKeys = new List<string>();

            var userPermissionKeys = user.Permissions.Select(permission => permission.Key);
            var userRolesPermissionKeys = user.Roles.SelectMany(role => role.Permissions.Select(permission => permission.Key));

            permissionKeys.AddRange(userPermissionKeys.Union(userRolesPermissionKeys));

            return permissionKeys;
        }

        /// <summary>
        /// This method return the SHA512 Hash of the password after salting, and return a Base64 representation of the resultig bytes
        /// </summary>
        /// <param name="password"></param>
        /// <returns>Base64 of the resulting Hash bytes</returns>
        private string GetPasswordHash(string password)
        {
            SHA512CryptoServiceProvider hashProvider = new SHA512CryptoServiceProvider();
            byte[] passwordHash = hashProvider.ComputeHash(UnicodeEncoding.Default.GetBytes(password + this.salt));
            string passwordHashBase64 = Convert.ToBase64String(passwordHash);

            return passwordHashBase64;
        }
    }

    /// <summary>
    /// This class is the exception used in the AuthenticationManager
    /// </summary>
    public class AuthenticationException : Exception
    {
        public AuthenticationException()
        {

        }

        public AuthenticationException(string message)
            : base(message)
        {

        }

        public AuthenticationException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
