﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMembership
{
    public partial class Role : IEquatable<Role>
    {
        public bool Equals(Role other)
        {
            if(this.Id == 0 && other.Id == 0)
            {
                throw new InvalidOperationException("Both entities are new");
            }
            else
            {
                return this.Id == other.Id;
            }
        }
    }
}
