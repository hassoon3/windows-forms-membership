USE [WindowsFormsMembership]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Username], [PasswordHash], [Name]) VALUES (2, N'admin', N'gh/lu2VAIiCoDXAPQDnsf4SlmZZ0Ah5z+JZMqiS9h0vA76mU6CndAoJJn4K7q2K9t7/a7oASenXr2Noqz7TrTg==', N'Administrator')
INSERT [dbo].[Users] ([Id], [Username], [PasswordHash], [Name]) VALUES (4, N'user', N'gh/lu2VAIiCoDXAPQDnsf4SlmZZ0Ah5z+JZMqiS9h0vA76mU6CndAoJJn4K7q2K9t7/a7oASenXr2Noqz7TrTg==', N'Normal User')
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[Permissions] ON 

INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (2, N'system.permission.read', N'Reading Permissions')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (3, N'system.role.read', N'Reading Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (4, N'system.role.update', N'Updating Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (5, N'system.role.create', N'Creating Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (6, N'system.role.delete', N'Deleting Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (7, N'system.user.read', N'Reading Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (8, N'system.user.update', N'Updating User')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (9, N'system.user.create', N'Creating Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (10, N'system.user.delete', N'Deleting Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (11, N'system.role.permission.update', N'Update Permissions of Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (13, N'system.user.role.update', N'Update Roles of Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (15, N'system.user.permission.update', N'Update Permissions to Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (18, N'system.role.permission.read', N'Read Permissions of Roles')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (20, N'system.user.permission.read', N'Read Permissions of Users')
INSERT [dbo].[Permissions] ([Id], [Key], [Description]) VALUES (21, N'system.user.role.read', N'Read Roles of Users')
SET IDENTITY_INSERT [dbo].[Permissions] OFF
INSERT [dbo].[UserPermission] ([Users_Id], [Permissions_Id]) VALUES (2, 3)
INSERT [dbo].[UserPermission] ([Users_Id], [Permissions_Id]) VALUES (2, 4)
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'Administrator')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'Role Manager')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[UserRole] ([Users_Id], [Roles_Id]) VALUES (2, 1)
INSERT [dbo].[UserRole] ([Users_Id], [Roles_Id]) VALUES (4, 2)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 2)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 3)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 3)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 4)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 4)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 5)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 5)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 6)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 6)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 7)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 8)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 9)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 10)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 11)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 11)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 13)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 15)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 18)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (2, 18)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 20)
INSERT [dbo].[RolePermission] ([Roles_Id], [Permissions_Id]) VALUES (1, 21)
