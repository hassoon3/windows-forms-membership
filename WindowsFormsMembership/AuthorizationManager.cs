﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMembership
{
    /// <summary>
    /// This class is a Singleton that is used to manage Permissions and Roles and the associations with Users
    /// </summary>
    public class AuthorizationManager
    {
        private static AuthorizationManager instance;
        /// <summary>
        /// Gets singleton instance
        /// </summary>
        public static AuthorizationManager Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new AuthorizationManager();
                }
                return instance;
            }
        }

        /// <summary>
        /// Gets a list of all the Permissions
        /// </summary>
        public IEnumerable<Permission> Permissions
        {
            get 
            {
                using(WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    return model.Permissions.Include("Roles").ToList();
                }
            }
        }

        /// <summary>
        /// Gets a list of all the Roles
        /// </summary>
        public IEnumerable<Role> Roles
        {
            get
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    return model.Roles.Include("Permissions").ToList();
                }
            }
        }

        /// <summary>
        /// Checks if the Authenticated User has a Permission
        /// </summary>
        /// <param name="key">The key of the Permission</param>
        /// <returns>True if User has the Permission, false otherwise</returns>
        public bool HasPermission(string key)
        {
            UserToken userToken = AuthenticationManager.UserTokenAuthenticated;

            if(userToken == null)
            {
                throw new InvalidOperationException("UserToken is not set, authorization failed");
            }

            return userToken.PermissionKeys.Contains(key) || userToken.IsSuperUser;
        }

        /// <summary>
        /// Updates User's manually assigned Permissions using the given set of Permission Ids
        /// </summary>
        /// <param name="userId">Id of the User</param>
        /// <param name="permissionIds">List of the Ids of the new collection of the User's Permissions</param>
        public void UpdateUserPermissions(int userId, IEnumerable<int> permissionIds)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var user = model.Users.SingleOrDefault(u => u.Id == userId);
                    if (user == null)
                    {
                        throw new AuthorizationException("User with id '" + userId.ToString() + "' does not exist");
                    }
                    else
                    {
                        user.Permissions.Clear();
                        model.Permissions.Where(permission => permissionIds.Contains(permission.Id))
                            .ToList()
                            .ForEach(permission => user.Permissions.Add(permission));
                        model.SaveChanges();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Updates Role's Permissions using the given set of Permission Ids
        /// </summary>
        /// <param name="roleId">The Id of the Role</param>
        /// <param name="permissionIds">List of the Ids of the new collection of the Role's Permissions</param>
        public void UpdateRolePermissions(int roleId, IEnumerable<int> permissionIds)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var role = model.Roles.SingleOrDefault(u => u.Id == roleId);
                    if (role == null)
                    {
                        throw new AuthorizationException("Role with id '" + roleId.ToString() + "' does not exist");
                    }
                    else
                    {
                        role.Permissions.Clear();
                        model.Permissions.Where(permission => permissionIds.Contains(permission.Id))
                            .ToList()
                            .ForEach(permission => role.Permissions.Add(permission));
                        model.SaveChanges();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Updates User's Roles using the given set of Role Ids
        /// </summary>
        /// <param name="userId">The Id of the User</param>
        /// <param name="roleIds">List of the Ids of the new collection of the User's Roles</param>
        public void UpdateUserRoles(int userId, IEnumerable<int> roleIds)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var user = model.Users.SingleOrDefault(u => u.Id == userId);
                    if (user == null)
                    {
                        throw new AuthorizationException("User with id '" + userId.ToString() + "' does not exist");
                    }
                    else
                    {
                        user.Roles.Clear();
                        model.Roles.Where(role => roleIds.Contains(role.Id))
                            .ToList()
                            .ForEach(role => user.Roles.Add(role));
                        model.SaveChanges();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Deletes a Role
        /// </summary>
        /// <param name="id">The Id of the Role</param>
        public void DeleteRole(int id)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var role = model.Roles.SingleOrDefault(u => u.Id == id);
                    if (role == null)
                    {
                        throw new AuthorizationException("Role with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        model.Roles.Remove(role);
                        model.SaveChanges();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Gets the Permissions of a Role
        /// </summary>
        /// <param name="id">The Id of the Role</param>
        /// <returns>A list of Permission objects associated to the Role</returns>
        public IEnumerable<Permission> GetRolePermissions(int id)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var role = model.Roles.SingleOrDefault(u => u.Id == id);
                    if (role == null)
                    {
                        throw new AuthorizationException("Role with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        return role.Permissions.ToList();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Gets the Permissions of a User
        /// </summary>
        /// <param name="id">The Id of the User</param>
        /// <returns>A list of Permission objects associated to the User</returns>
        public IEnumerable<Permission> GetUserPermissions(int id)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var user = model.Users.SingleOrDefault(u => u.Id == id);
                    if (user == null)
                    {
                        throw new AuthorizationException("User with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        return user.Permissions.ToList();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Gets the Roles of a User
        /// </summary>
        /// <param name="id">The Id of the User</param>
        /// <returns>A list of Role objects associated to the Role</returns>
        public IEnumerable<Role> GetUserRoles(int id)
        {
            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var user = model.Users.Include("Roles.Permissions").SingleOrDefault(u => u.Id == id);
                    if (user == null)
                    {
                        throw new AuthorizationException("User with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        return user.Roles.ToList();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error", exp);
            }
        }

        /// <summary>
        /// Creates a new Role
        /// </summary>
        /// <param name="name">The name of the Role</param>
        /// <param name="permissionDefaultKeys">The keys of the initial Role Permissions</param>
        /// <returns>The Id of the newly created Role</returns>
        public int CreateRole(string name, IEnumerable<string> permissionDefaultKeys = null)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("Arguments can not be null");
            }

            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    if (model.Roles.Count(u => u.Name == name) > 0)
                    {
                        throw new AuthorizationException("Role Name already exist.");
                    }

                    Role role = new Role();
                    role.Name = name;
                    
                    //Associate permissions
                    if (permissionDefaultKeys != null)
                    {
                        var permissionDefaults = model.Permissions.Where(permission => permissionDefaultKeys.Contains(permission.Key));
                        
                        permissionDefaults.ToList().ForEach(permission => role.Permissions.Add(permission));
                    }

                    model.Roles.Add(role);
                    model.SaveChanges();

                    return role.Id;
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General Error, see inner exception", exp);
            }
        }

        /// <summary>
        /// Updates a Role
        /// </summary>
        /// <param name="id">The Id of the Role</param>
        /// <param name="name">The new name of the Role</param>
        public void UpdateRole(int id, string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException("name", "Name can not be null");
            }

            try
            {
                using (WindowsFormsMembershipContainer model = new WindowsFormsMembershipContainer())
                {
                    var role = model.Roles.SingleOrDefault(r => r.Id == id);
                    if (role == null)
                    {
                        throw new AuthorizationException("Role with id '" + id.ToString() + "' does not exist");
                    }
                    else
                    {
                        role.Name = name;

                        model.SaveChanges();
                    }
                }
            }
            catch (AuthorizationException)
            {
                throw;
            }
            catch (Exception exp)
            {
                throw new AuthorizationException("General error", exp);
            }
        }
    }

    /// <summary>
    /// This class is the exception used in the AuthorizationManager
    /// </summary>
    public class AuthorizationException : Exception
    {
        public AuthorizationException()
        {

        }

        public AuthorizationException(string message)
            : base(message)
        {

        }

        public AuthorizationException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
