﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsMembership
{
    public class UserToken
    {
        public bool IsSuperUser { get; private set; }

        public IEnumerable<string> PermissionKeys { get; private set; }

        public int UserId { get; private set; }

        public UserToken(int userId, IEnumerable<string> permissionKeys, bool isSuperUser = false)
        {
            this.IsSuperUser = isSuperUser;
            this.UserId = userId;
            this.PermissionKeys = permissionKeys;
        }
    }
}
